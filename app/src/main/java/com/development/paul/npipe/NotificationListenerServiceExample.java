package com.development.paul.npipe;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import java.sql.Types;

import static android.content.ContentValues.TAG;


/**
 * Created by paul on 4/4/18.
 * Notif
 */

public class NotificationListenerServiceExample extends NotificationListenerService {

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn){

        String packName = sbn.getPackageName();
        String text="";
        String title="";

        Bundle extras = sbn.getNotification().extras;
        System.out.println(extras);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            title = extras.getString("android.title");
            text = String.valueOf(extras.getCharSequence("android.text"));
        }

        Request request = new Request();
        request.send(packName, title, text);
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn){
    }
}