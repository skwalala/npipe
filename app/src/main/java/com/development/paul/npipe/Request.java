package com.development.paul.npipe;

import android.widget.TextView;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import static android.content.Context.SYSTEM_HEALTH_SERVICE;
import static com.development.paul.npipe.MainActivity.ip;
import static com.development.paul.npipe.MainActivity.port;
import static com.development.paul.npipe.MainActivity.username;
import static com.development.paul.npipe.MainActivity.Hpassword;
import static com.development.paul.npipe.MainActivity.run;


/**
 * Created by paul on 3/10/18.
 * server to listen connection and send a answer
 */

public class Request {

    Request(){
    }


    public void send(String packageName, String title, String text){

        byte[] packageNameByte;
        byte[] titleByte;
        byte[] textByte;
        byte[] dataSpliter = "$#".getBytes();

        System.out.println("notification detected");
        System.out.println(packageName);
        System.out.println(title);
        System.out.println(text);
        if (run && packageName!=null && title!=null && text!=null){

            packageNameByte = packageName.getBytes();
            titleByte = title.getBytes();
            textByte = text.getBytes();

            try {
                //création de la connection
                Socket s = new Socket(ip, port);

                //création d'un out buffer et d'un in buffer
                BufferedOutputStream buf = new BufferedOutputStream(new PrintStream(s.getOutputStream()));
                BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));

                buf.write(packageNameByte);
                buf.write(dataSpliter);
                buf.write(titleByte);
                buf.write(dataSpliter);
                buf.write(textByte); //ecriture dans le buffer
                buf.flush(); //flush du buffer


            } catch (UnknownHostException e){
                e.printStackTrace();
                System.out.println();
                System.out.println();
                System.out.println();
                System.out.println();
                System.out.println("erreur de connection");
                System.out.println();
                System.out.println();
                System.out.println();
                System.out.println();
                System.out.println();
            } catch (IOException e){
                e.printStackTrace();
            }

        }

    }
}
