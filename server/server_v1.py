import socket
from gi.repository import Notify

hote = ''
port = 12800

connexion_principale = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
connexion_principale.bind((hote, port))
connexion_principale.listen(5)
print("Le serveur ecoute a present sur le port {}".format(port))

running = True
while running:
	connexion_avec_client, infos_connexion = connexion_principale.accept()

	msg_recu = b""
	while msg_recu != b"fin":
	    msg_recu = connexion_avec_client.recv(1024)
	    # L'instruction ci-dessous peut lever une exception si le message
	    if msg_recu.decode()!="fin":
		    print(msg_recu.decode())
		    Notify.init("Npipe")
		    Notify.Notification.new(msg_recu.decode()).show()
		    connexion_avec_client.send(b"recived")

	print("Fermeture de la connexion")
	connexion_avec_client.close()

connexion_principale.close()
