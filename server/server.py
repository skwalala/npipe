#!/usr/bin/python
 # -*- coding: utf-8 -*-
import socket
from gi.repository import Notify

hote = ''
port = 12800

connexion_principale = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
connexion_principale.bind((hote, port))
connexion_principale.listen(5)
print("Le serveur ecoute a present sur le port {}".format(port))

running = True
while running:
	connexion_avec_client, infos_connexion = connexion_principale.accept()

	msg_recu = b""
	msg_recu = connexion_avec_client.recv(1024)
	data=msg_recu.decode('utf-8')
	print(data)
	splitData=data.split("$#")

	# L'instruction ci-dessous peut lever une exception si le message
	if splitData[0]!="pedometer.steptracker.calorieburner.stepcounter" and splitData[0]!="":
		Notify.init("Npipe")
		Notify.Notification.new(
		splitData[1],	#title
		splitData[2]	#message
		).show()

	connexion_avec_client.send(b"recived")
	connexion_avec_client.close()

connexion_principale.close()
